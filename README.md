Chaos
=====

As the name implies - this is a public repository with my Chaotica params.

All chaos files inside are licensed under [CC BY-NC-SA 4.0][license]

[license]: https://creativecommons.org/licenses/by-nc-sa/4.0/ "Creative Commons license"
